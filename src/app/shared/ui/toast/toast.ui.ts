import { Injectable } from '@angular/core';
import toastr from 'toastr';
import { ToastOptions } from './toast-options.model';

@Injectable({
  providedIn: 'root'
})
export class ToastUI {

  constructor() { }

  success(message: string, title: string = '', position = 'toast-top-right', duration = 3000) {
    toastr.success(message, title, {
      closeButton: true,
      progressBar: true,
      timeOut: duration,
      positionClass: position
    });
  }

  info(message: string, title: string = '', position = 'toast-top-right', duration = 3000) {
    toastr.info(message, title, {
      closeButton: true,
      progressBar: true,
      timeOut: duration,
      positionClass: position
    });
  }

  warning(message: string, title: string = '', position = 'toast-top-right', duration = 3000) {
    toastr.warning(message, title, {
      closeButton: true,
      progressBar: true,
      timeOut: duration,
      positionClass: position
    });
  }

  error(message: string, title: string = '', position = 'toast-top-right', duration = 3000) {
    toastr.error(message, title, {
      closeButton: true,
      progressBar: true,
      timeOut: duration,
      positionClass: position
    });
  }

  toastWithOptions(options: ToastOptions) {
    if (options.type == 'success')
      toastr.success(options.message, options.header, options);
    else if (options.type == 'error')
      toastr.error(options.message, options.header, options);
    else if (options.type == 'warning')
      toastr.warning(options.message, options.header, options);
    else
      toastr.info(options.message, options.header, options);
  }

}
