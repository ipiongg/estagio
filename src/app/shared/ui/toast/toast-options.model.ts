export class ToastOptions {

  constructor(
    public type: string = 'info',
    public header?: string,
    public message?: string,
    public closeButton: boolean = true,
    public debug?: boolean,
    public newestOnTop?: boolean,
    public progressBar: boolean = true,
    public positionClass: string = 'toast-top-right',
    public preventDuplicates?: boolean,
    public onclick?: any,
    public showDuration?: number,
    public hideDuration?: number,
    public timeOut: number = 3000,
    public extendedTimeOut?: number,
    public showEasing?: string,
    public hideEasing?: string,
    public showMethod?: string,
    public hideMethod?: string
  ) { }

  // https://codeseven.github.io/toastr/demo.html
}