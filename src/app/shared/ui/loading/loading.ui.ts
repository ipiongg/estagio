// import { Injectable } from '@angular/core';
// import { LoadingController } from '@ionic/angular';

// @Injectable({
//   providedIn: 'root'
// })
// export class LoadingUI {

//   public loading: any;

//   constructor(private loadingCtrl: LoadingController) { }

//   async presentLoading(message: string = "Carregando") {
//     await this.loadingCtrl.getTop().then(async (response: any) => {
//       if (!response) {
//         const loading = await this.loadingCtrl.create({
//           message: message
//         });
//         await loading.present();
//       }
//     });
//   }

//   async dismiss() {
//     await this.loadingCtrl.getTop().then(async (response: any) => {
//       if (response)
//         await this.loadingCtrl.dismiss();
//     });
//   }

// }
