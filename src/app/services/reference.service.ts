import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReferenceService {

  constructor(private http: HttpClient) { }

  createReference(id, dados): Promise<any> {
    return this.http
      .put(`${environment.apiUrl}trabalhos/${id}`, dados)
      .toPromise();
  }
}
