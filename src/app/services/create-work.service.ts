import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CreateWorkService {

  constructor(private http: HttpClient) { }

  private _refreshNeeded$ = new Subject<void>();

  get _refreshNeeded() {
    return this._refreshNeeded$;
  }

  createWork(dados): Promise<any> {
    return this.http
      .post(`${environment.apiUrl}trabalhos/`, dados)
      .pipe(
        tap(() => {
          this._refreshNeeded$.next();
        })
      )
      .toPromise();
  }

  getWork(): Promise<any> {
    return this.http.get(`${environment.apiUrl}trabalhos/`)
      .toPromise()
  }

  deletWork(id): Promise<any> {
    return this.http
      .delete(`${environment.apiUrl}trabalhos/${id}`)
      .pipe(tap(() => {
        this._refreshNeeded$.next();
      })).toPromise()
  }
}
