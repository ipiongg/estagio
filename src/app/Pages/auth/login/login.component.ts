import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form: FormGroup;
  public form2: FormGroup;

  constructor(
    private service: LoginService,
    private route: Router,
    private formBuilder: FormBuilder,
    public toast: ToastUI
  ) { }

  ngOnInit(): void {
    this.buildForm()
    this.buildForm2()
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    })
  }

  private buildForm2() {
    this.form2 = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
    })
  }

  submit() {
    if (this.form.valid)
      this.login()

    else
      this.toast.error('Dados invalidos')
    return
  }

  login() {
    this.service.login(this.form.value)
      .then((data) => {
        console.log("logou", data.user)
        if (data)
          this.toast.success('Bem vindo!')
        this.route.navigateByUrl('/dashboard')
        localStorage.setItem('reference-token', data.token)
        JSON.stringify(localStorage.setItem('user', data.user))
      })
      .catch((error) => {
        this.toast.error('Nome de usuário ou senha invalidos')
      })
  }
}
