import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VisualizarRoutingModule } from './visualizar-routing.module';
import { VisualizarComponent } from './visualizar.component';
import { SideMenuModule } from 'src/app/components/frame/side-menu/side-menu.module';


@NgModule({
  declarations: [VisualizarComponent],
  imports: [
    CommonModule,
    VisualizarRoutingModule,
    SideMenuModule
  ]
})
export class VisualizarModule { }
