import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';

@Component({
  selector: 'app-visualizar',
  templateUrl: './visualizar.component.html',
  styleUrls: ['./visualizar.component.scss']
})
export class VisualizarComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private toast: ToastUI,
    private router: Router,


  ) { }

  referencias_list: any = []
  works: any

  ngOnInit(): void {
    this.works = JSON.parse(this.route.snapshot.params.dates);
    this.referencias_list = this.works.referencias;

    console.log('referencias', this.referencias_list)
  }

  createReference() {
    this.router.navigate(['/criar', { dates: JSON.stringify(this.works) }])
  }

}
