import { Component, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ReferenceService } from 'src/app/services/reference.service';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';



@Component({
  selector: 'app-criar',
  templateUrl: './criar.component.html',
  styleUrls: ['./criar.component.scss'],
})
export class CriarComponent implements OnInit {
  model: NgbDateStruct;

  constructor(
    private formBuilder: FormBuilder,
    private ngbCalendar: NgbCalendar, private dateAdapter: NgbDateAdapter<string>,
    private service: ReferenceService,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastUI
  ) { }

  public referencias: any = []
  public form: FormGroup;
  public type_reference: any
  public alert: boolean = false
  public meio_publicacao: any;
  public works: any;


  ngOnInit(): void {
    this.buildForm()
    this.works = JSON.parse(this.route.snapshot.params.dates);
    // console.log("o work", this.works)
    this.getMonth()
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      firstname: ['', Validators.compose([])],
      lastname: ['', Validators.compose([])],
      article_title: ['', Validators.compose([Validators.required])],
      subtitle: ['', Validators.compose([])],
      periodic: ['', Validators.compose([Validators.required])],
      location: ['', Validators.compose([])],
      public_date: ['', Validators.compose([])],
      volume: ['', Validators.compose([])],
      edition_number: ['', Validators.compose([])],
      first_page: ['', Validators.compose([Validators.required])],
      last_page: ['', Validators.compose([Validators.required])],
      available_at: [''],
      acces_at: ['']
    })
  }

  add() {
    // var input = document.createElement("input");
    // input.type = "text";
    // input.className = "css-class-name"; // set the CSS class
    // input.innerHTML = ` 
    //   <input formControlName="firstname" class="uk-input" id="form-stacked-text" type="text">`;
    // document.querySelector('.showInputField').appendChild(input);
    let row = document.createElement('div');
    row.className = 'col-sm';
    row.innerHTML = ` 
      <input formControlName="firstname" class="uk-input" id="form-stacked-text" type="text">`;
    document.querySelector('.showInputField').appendChild(row);
  }

  changeMedium(event) {
    console.log(event)
  }

  changeType(event) {
    console.log(event)
  }

  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  getMonth() {
    var months = ["Jan.", "Fev.", "Mar.", "Abr.", "Maio", "Jun.", "Jul.", "Ago.", "Set.", "Out.", "Nov.", "Dez."];
    var d = new Date('12-12-2020');
    var monthName = months[d.getMonth()]; // "July" (or current month)
    console.log('MEs', monthName)
  }

  /**
COM AUTOR
 * SOBRENOME, Nome. Título da matéria. Nome do jornal,
 cidade de publicação, dia, mês e ano. Seção (se houver).
 Disponível em: <URL>. Acesso em: dia, mês e ano.
 */

  /**
SEM AUTOR
 *TÍTULO da matéria. Nome do jornal, cidade de publicação,
 dia, mês e ano. Seção (se houver). Disponível em: <URL>.
 Acesso em: dia, mês e ano.
 */
  formatArtigo() {

    console.log(this.form.value.public_date)
    var lastname = ''
    var fisrtname = ''
    var title = ''
    var periodic = ''
    var location = ''
    var public_date = ''
    var available_at = ''
    var access_at = ''
    var subtitle = ''
    var volume = ''
    var edition_number = ''
    var first_page = ''
    var last_page = ''

    if (this.form.get('lastname').value) {
      lastname = this.form.get('lastname').value.toUpperCase() + ', '
    }

    if (this.form.get('firstname').value) {
      fisrtname = this.form.get('firstname').value + '. '
    }

    if (this.form.get('article_title').value) {
      title = this.form.get('article_title').value + '. '
    }

    if (this.form.get('periodic').value) {
      periodic = this.form.get('periodic').value + ', '
    }

    if (this.form.get('location').value) {
      location = this.form.get('location').value + ', '
    }

    if (this.form.get('public_date').value) {
      public_date = this.form.get('public_date').value
    }

    if (this.form.get('available_at').value) {
      available_at = `Disponível em: <${this.form.get('available_at').value}>.`
    }

    if (this.form.get('acces_at').value) {
      access_at = 'Acesso em:' + this.form.get('acces_at').value
    }

    if (this.form.get('subtitle').value) {
      subtitle = this.form.get('subtitle').value + '. '
    }

    if (this.form.get('volume').value) {
      volume = 'v. ' + this.form.get('volume').value + ', '
    }

    if (this.form.get('edition_number').value) {
      edition_number = 'n. ' + this.form.get('edition_number').value + ', '
    }

    if (this.form.get('first_page').value) {
      first_page = 'p. ' + this.form.get('first_page').value + '-'
    }

    if (this.form.get('last_page').value) {
      last_page = this.form.get('last_page').value + ', '
    }

    if (this.meio_publicacao == 'impresso')
      var formatado = lastname + fisrtname + title + subtitle + periodic + location + volume + edition_number + first_page + last_page + public_date

    else
      var formatado = lastname + fisrtname + title + subtitle + periodic + location + volume + edition_number + first_page + last_page + public_date + available_at + access_at

    const final_format = this.works.referencias.push(formatado)
    console.log("formatado", formatado)
    console.log('Final format', this.works)

    this.service.createReference(this.works.id, this.works).then((data) => {
      console.log(data)
      if (data) {
        this.toast.success('Referência criada com sucesso')
        this.router.navigateByUrl('/dashboard')
      }
      else
        this.toast.error('Erro ao criar referência, verifque todos os campos')
      return
    })
      .catch((error) => {
        if (error)
          this.toast.error('Erro ao criar referência, verifque todos os campos')
      })
  }

  formatSite() {
    console.log('Formata Site')

  }


}
