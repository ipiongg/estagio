import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CriarRoutingModule } from './criar-routing.module';
import { CriarComponent } from './criar.component';
import { SideMenuModule } from 'src/app/components/frame/side-menu/side-menu.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedComponentsModule } from 'src/app/components/shared/shared-components.module';
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule, IConfig } from 'ngx-mask'

const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [CriarComponent],
  imports: [
    CommonModule,
    CriarRoutingModule,
    SideMenuModule,
    SharedComponentsModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(maskConfig),
    NgbModule

  ]
})
export class CriarModule { }
