import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CriarComponent } from './criar.component';

const routes: Routes = [{ path: '', component: CriarComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriarRoutingModule { }
