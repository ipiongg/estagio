import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public form: FormGroup;

  constructor(
    private service: RegisterService,
    private route: Router,
    private formBuilder: FormBuilder,
    public toast: ToastUI
  ) { }

  ngOnInit(): void {
    this.buildForm()
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      first_name: ['', Validators.compose([Validators.required])],
      // last_name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    })

  }

  register() {
    this.service.register(this.form.value).then((data) => {
      if (data)
        this.toast.success('Usuário criado com sucesso')
      this.route.navigateByUrl('/login')

    })
      .catch((error) => {

        if (error.error.username)
          this.toast.error('Este nome de usuário já está em uso')
        else
          this.toast.error('Este email já esta em uso')
        console.log('error', error)
      })
  }

}
