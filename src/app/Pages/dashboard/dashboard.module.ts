import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NbLayoutModule } from '@nebular/theme';
import { SideMenuModule } from 'src/app/components/frame/side-menu/side-menu.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NbLayoutModule,
    SideMenuModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
