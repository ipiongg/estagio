import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateWorkService } from 'src/app/services/create-work.service';
import { formatDate } from '@angular/common';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  list_works: any = []
  currenct_date: any;
  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: CreateWorkService,
    private route: Router,
    private toast: ToastUI

  ) { }

  ngOnInit(): void {
    this.buildForm()

    this.service._refreshNeeded.subscribe(() => {
      this.buscarTrabalho()
    });

    this.buscarTrabalho()

  }

  private buildForm() {

    this.form = this.formBuilder.group({
      nome: ['', Validators.compose([Validators.required])],
      data_criacao: [formatDate(new Date(), 'yyyy-MM-dd', 'en'), Validators.compose([Validators.required])],
      referencias: [['Você não tem nenhuma referência'], Validators.compose([Validators.required])]
    })

  }

  criarTrabalho() {
    this.service.createWork(this.form.value).then((data) => {
      if (data)
        this.toast.success('Trabalho criado com sucesso!')
    })
      .catch((error) => {
        console.log(error)
        if (error.error.nome)
          this.toast.error('É nescessário inserir um nome ao trabalho')
        else
          this.toast.error('Erro ao criar trabalho')
      })
  }

  buscarTrabalho() {
    this.service.getWork().then((data) => {
      this.list_works = data
      console.log('lista', this.list_works)
    })
  }

  visualizar(trabalho) {
    console.log('o trabalho', trabalho)
    this.route.navigate(['/visualizar', { dates: JSON.stringify(trabalho) }])
  }

  excluirTrabalho(id) {
    this.service.deletWork(id).then((data) => {
      window.alert('Trabalho deletado com sucesso!')
      console.log("deletou o cara", data)
    })
  }

}
