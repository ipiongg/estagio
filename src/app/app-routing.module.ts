import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "./guards/auth.guard";



const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: '', loadChildren: () => import('./Pages/home/home.module').then(m => m.HomeModule) },
  { path: 'login', loadChildren: () => import('./Pages/auth/login/login.module').then(m => m.LoginModule) },
  {
    path: 'dashboard', loadChildren: () => import('./Pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard],
  },
  // { path: 'side-menu', loadChildren: () => import('./components/frame/side-menu/side-menu.module').then(m => m.SideMenuModule) },
  {
    path: 'visualizar', loadChildren: () => import('./Pages/referencias/visualizar/visualizar.module').then(m => m.VisualizarModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'criar', loadChildren: () => import('./Pages/referencias/criar/criar.module').then(m => m.CriarModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'editar', loadChildren: () => import('./Pages/referencias/editar/editar.module').then(m => m.EditarModule),
    canActivate: [AuthGuard],
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
