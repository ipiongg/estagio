import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ValidationMessageComponent } from './validation-message/validation-message.component';


@NgModule({
  declarations: [

    ValidationMessageComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    // MatProgressSpinnerModule
  ],
  exports: [
    ValidationMessageComponent,
  ]
})
export class SharedComponentsModule { }
