import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SideMenuRoutingModule } from './side-menu-routing.module';
import { SideMenuComponent } from './side-menu.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [SideMenuComponent],
  imports: [
    CommonModule,
    SideMenuRoutingModule,
    RouterModule
  ],
  exports: [
    SideMenuComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SideMenuModule { }
