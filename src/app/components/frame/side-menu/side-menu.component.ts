import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  public isCollapsed: any;
  constructor(private router: Router,
  ) { }

  ngOnInit(): void {
  }

  logout() {
    localStorage.removeItem('reference-token')
    localStorage.removeItem('user')
    this.router.navigateByUrl('/')
  }



}
